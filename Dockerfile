FROM registry.esss.lu.se/ics-docker/miniconda:latest as build

ARG RECSYNC_VERSION=1.4

USER root
RUN yum install -y gcc-c++ make

USER conda
RUN git clone --depth 1 --branch $RECSYNC_VERSION https://github.com/ChannelFinder/recsync.git /tmp/recsync
RUN conda create -n epics -c conda-e3-virtual epics-base gxx_linux-64
RUN echo "conda activate epics" >> ~/.bashrc
WORKDIR /tmp/recsync/client
COPY arch.patch /tmp/
RUN git apply /tmp/arch.patch
COPY --chown=conda:users RELEASE /tmp/recsync/client/configure/RELEASE
RUN source ~/.bashrc && make && make install

FROM registry.esss.lu.se/ics-docker/miniconda:latest

USER root
COPY --from=build /opt/conda/envs/epics /opt/conda/envs/epics

USER conda
COPY --from=build /tmp/recsync/client /home/conda/

WORKDIR /home/conda/iocBoot/iocdemo
ENV LD_LIBRARY_PATH=/home/conda/lib/linux-x86_64
CMD ./st.cmd
